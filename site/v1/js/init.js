function initAllSwitch(mode) {

  switchs.forEach(function (element) {
    let switche = stage.findOne("#" + element);
    switche.on('click touchstart', function () {
      click++;
      activeSwitch(switche.id());
      layer.draw();
      lineUpdate(element);
      checkEnd(mode);
    });
    switche.on('mouseover', function () {
      document.body.style.cursor = "pointer";
    });
    switche.on('mouseout', function () {
      document.body.style.cursor = "default";
    });
    //faire le random de l'inversion de switch
    if (isTutoriel && getRandomArbitrary(0, 3) == 1) {
      activeSwitch(switche.id());
      if (checkIfEnd())
        activeSwitch(switche.id());
    }

  });
}

function initLayer() {

}

function compareLogiqueForEnd(a, b) {
  if (a.y > b.y)
    return -1;
  if (a.y < b.y)
    return 1;
  if (a.y == b.y) {
    if (a.x > b.x)
      return -1;
    else return 1;
  }
  return 0;
}

function initEnd() {
  var listeLogique = [];
  let countLogique = 0;
  logiques.forEach(function (element) {
    if (element.id3.length === 0) {
      countLogique++;
      listeLogique.push(element);
    }
  });
  listeLogique.sort(compareLogiqueForEnd);
  end.position = countLogique;
  end.let = countLogique;
  listeLogique.forEach(function (element) {
    createLink(element, end);
  });
  switchs.forEach(function (element) {
    lineUpdate(element);
  })
}