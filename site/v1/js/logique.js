function findLogique(logiqueId) {
    let elem = null;
    logiques.forEach(function (element) {
        if (element.name === logiqueId) {
            elem = element;
        }
    });
    return elem;
}

function giveSwitchYWithLineId(lineId) {
    var elementY = null;
    switchsInfoCopy.forEach(function (element) {
        if (stage.findOne("#" + element.id).id3 == lineId) {
            elementY = (element.y);
        }
    });
    return elementY;
}

function removeLineFromSwitch(lineId) {
    switchsInfoCopy.forEach(function (element) {
        element.id3.forEach(function (line) {
            if (line == lineId) {
                ax = element.id3.indexOf(lineId);
                element.id3.splice(ax, 1);
            }
        });

    });
}
function removeLineFromLogique(lineId) {
    logiques.forEach(function (element) {
        element.id3.forEach(function (line) {
            if (line == lineId) {
                ax = element.id3.indexOf(lineId);
                element.id3.splice(ax, 1);
            }
        });
    });
}

function removeLine(lineId) {
    //remove from switch
    if (getSwitchFromLine(lineId) != null)
        removeLineFromSwitch(lineId);
    else
        removeLineFromLogique(lineId);
    let line = stage.findOne("#" + lineId);
    line.destroy();

}

function giveLineId(logiqueId, entre) {
    var newEntre = entre;
    isSwitch = false;
    if (entre.id !== undefined) {
        switchsInfoCopy.forEach(function (element) {
            if (entre.id() == element.id) {
                isSwitch = true;
                newEntre = element;
            }
        });
    }
    let lineId = null;
    let logique = findLogique(logiqueId);
    if (logique.y >= newEntre.y) {
        if (logique.type === "inv") {
            if (logique.id1 == null) {
                lineId = "id1";
            }
        }
        else {
            if (logique.id1 == null) {
                lineId = "id1";
            } else if (logique.id2 == null) {
                lineId = "id2";
                /*if(isSwitch){
                    if(giveSwitchYWithLineId(logique.id1) > entre.getY()){
                        console.log(giveSwitchYWithLineId(logique.id1));
                        console.log(entre);
                        removeLine(entre.id3);
                        lineRemove.push([entre, "switch"]);
                    }else{
                        lineId = "id2";
                    }
                }*/
            }
        }
    } else {
        if (logique.type === "inv") {
            if (logique.id1 == null) {
                lineId = "id1";
            }
        }
        else {
            if (logique.id2 == null) {
                lineId = "id2";
            } else if (logique.id1 == null) {
                lineId = "id1";
            }


        }
    }

    return lineId
}

function checkLogiqueLines(logiqueId, lineId) {
    let logique = findLogique(logiqueId);
    logiques.forEach(function (element, index) {
        if (element.name === logique.name) {
            Object.keys(element).map(function (objectKey, index) {
                if (objectKey == lineId && objectKey != null) {
                    return true;
                }
            });
        }
    });
    return false;
}