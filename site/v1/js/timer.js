function initTimer(mode) {
  if (mode === undefined || mode === null || mode !== "tuto")
    document.querySelector(".timer_txt").innerHTML = timeEnd - timer.getTimeValues().seconds;
  else
    document.querySelector(".timer_txt").innerHTML = "∞";
  document.querySelector(".progress_bar").style.animationPlayState = 'paused';
}

function move() {
  document.querySelector(".timer_txt").innerHTML = timeEnd - timer.getTimeValues().seconds;
  switch (timeEnd - timer.getTimeValues().seconds) {
    case 5:
    case 4:
      document.querySelector(".timer_txt").style.color = "#eca1a6";
      break;
    case 3:
    case 2:
    case 1:
    case 0:
      document.querySelector(".timer_txt").style.color = "#c94c4c";
      break;
    default:
      document.querySelector(".timer_txt").style.color = document.querySelector("#modeDeJeu").style.color;
  }
}

function demarrerTimer() {
  timer.start();
  document.querySelector(".progress_bar").style.animationDuration = timeEnd + "s";
  document.querySelector(".progress_bar").style.animationPlayState = 'running';
}

var timer = new easytimer.Timer();
timer.start();
timer.addEventListener('secondsUpdated', function (e) {
  if (timer.getTimeValues().seconds <= timeEnd) {

    move();
  } else {

    timeTot = timeTot + timer.getTimeValues().seconds;
    endTime();
    timer.stop();
  }
});

function endTime() {
  console.log(modeG);
  document.querySelector(".nivShow").innerHTML = " / 5";
  if (modeG === "vanillaInfini" || modeG === "normalInfini") {
    clickTot = clickTot + click;
    var scoreTot
    if (niveauActuel === 1)
      scoreTot = 0;
    else
      scoreTot = ((1 / timeTot) * (1 / clickTot) * 100000).toFixed(2);
    document.querySelector("#scoreTotGameOver").innerHTML = "Score : " + scoreTot;
    document.querySelector("#niveauGameOver").innerHTML = niveauActuel;
    document.querySelector("#TempsTotGameOver").innerHTML = "Temps : " + timeTot;
    sendScore(scoreTot, modeG, clickTot, niveauActuel, 25, timeTot);
    clickTot = 0;
    document.querySelector(".nivShow").innerHTML = "";
    document.querySelector("#lose_pannel").style.display = "block";
    document.querySelector("#play_menu_game").style.display = "none";
  } else {
    if (!isTuto) {
      document.querySelector("#scoreTotGameOver").innerHTML = "Vous devez réussir tout les niveaux pour être classé";
      document.querySelector("#niveauGameOver").innerHTML = "";
      document.querySelector("#TempsTotGameOver").innerHTML = "";
      document.querySelector("#lose_pannel").style.display = "block";
      document.querySelector("#play_menu_game").style.display = "none";
      document.querySelector("#niveauGameOver").innerHTML = niveauActuel;
    }
  }
  isTuto = true;
  document.querySelector("#retry_button").onclick = function () {
    wait(modeG, document.querySelector(".listenerHover"));
    document.querySelector("#lose_pannel").style.display = "none";
    document.querySelector("#play_menu_game").style.display = "block";
  }

}

function resetStar() {
  document.querySelector("#star1").style.color = document.body.style.color;
  document.querySelector("#star2").style.color = document.body.style.color;
  document.querySelector("#star3").style.color = document.body.style.color;
  document.querySelector("#star4").style.color = document.body.style.color;
  document.querySelector("#star5").style.color = document.body.style.color;
}