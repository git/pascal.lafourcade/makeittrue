class GameData {

  hauteur;
  largeur;
  timeEnd;
  numberColonne1;
  numberColonne2;
  isEtNonAllowed;
  isOuNonAllowed;
  niveau;
  isTuto;
  mode;
  container;
  colonneTot;

  constructor(hauteur, largeur, colonneTot, timeEnd, mode, isTuto, container) {
    this.hauteur = hauteur;
    this.largeur = largeur;
    this.timeEnd = timeEnd;
    this.numberColonne1 = 2;
    this.numberColonne2 = 1;
    this.isEtNonAllowed = false;
    this.isOuNonAllowed = false;
    this.niveau = 1;
    this.mode = mode;
    this.isTuto = isTuto;
    this.container = container;
    this.colonneTot = colonneTot;
  }

  createStageCanvas() {
    stage = new Konva.Stage({
      container: this.container,
      /*rotation: -90,
      x: 20,
      y: 1000,*/
      width: this.hauteur,
      height: this.largeur,
    });
    stage.add(layer);
  }

  generateRandomDoor() {
    numberPerColonne = [this.numberColonne1, this.numberColonne2];
    var logiqueCount = 0;

    for (let i = 0; i < this.colonneTot; i++) {
      liveColonneNumber.push([]);
    }
    for (let i = 0; i < this.colonneTot; i++) {
      for (let j = 0; j < numberPerColonne[i]; j++) {
        logiqueCount++;
        let type = null;
        switch (getRandomArbitrary(0, 4)) {
          case 0:
            type = "et";
            break;
          case 1:
            type = "ou";
            break;
          case 2:
            if (this.isEtNonAllowed) {
              type = "etnon";
            } else {
              type = "et";
            }
            break;
          case 3:
            if (this.isOuNonAllowed) {
              type = "nonou";
            } else {
              type = "et";
            }
            break;
        }
        insertLogiqueColonne("logique" + logiqueCount, type, i);
      }
    }
    logiques.forEach(function (element) {
      createLinkAuto(element.name);
    });
  }

  

  setDifficulty(niveau) {
    switch (niveau) {
      case 1:
      case 2:
        timeEnd = 5;
        break;
      case 3:
      case 4:
        timeEnd = 4;
        break;
      case 5:
        timeEnd = 3;
        break;
      case 6:
      case 7:
      case 8:
        isEtNonAllowed = true;
        timeEnd = 5;
        numberColonne1 = 3;
        break;
      case 9:
      case 10:
      case 11:
        isEtNonAllowed = true;
        timeEnd = 4;
        numberColonne1 = 3;
        break;
      case 12:
      case 13:
      case 14:
        isEtNonAllowed = true;
        isOuNonAllowed = true;
        timeEnd = 4;
        numberColonne1 = 3;
        break;
      default:
        isEtNonAllowed = true;
        isOuNonAllowed = true;
        timeEnd = 3;
        numberColonne1 = 4;
        break;
    }
  }
  createGame(){

    this.setDifficulty(this.niveau);
    
    initTimer();
    this.createStageCanvas();
    this.generateRandomDoor();

    calculNombreSwitch();
    switchCreator(numberOfSwitch);
    createAllLinkSwitch();

    initAllSwitch();
    createEnd();
    initEnd();

    checkAllSortieLogique();
  }
}

function createSwitch(id, x, y) {
  var switche = {
    id: id,
    x: x,
    y: y,
  };
  switchsInfo.push(switche);
  switchsInfoCopy.push(switche);
  switchs.push(id);
  var num = lineCount.length;
  lineCount.push(1);
  var rect1 = new Konva.Rect({
    x: x,
    y: y,
    width: SwitchWidth,
    height: SwitchHeight,
    stroke: colorSwitchBorder,
    fill: colorSwitchInnactiveBackground,
    strokeWidth: 4,
    id: id,
    id3: null,
  });
  layer.add(rect1);
}