function initAll() {
    loadData();
    darkMode();
    setLang("fr");
    tutoCreateLayer();
    dysFont();
}

function loadData() {
    pseudo = localStorage.getItem("pseudo");
    colorLineInnactive = localStorage.getItem("colorLineInnactive");
    if (pseudo === null || pseudo === undefined) {
        document.querySelector(".alert").style.display = "block";
        pseudo = generateName();
    }
    if (colorLineInnactive === null || colorLineInnactive === undefined) {
        localStorage.setItem("pseudo", pseudo);
        localStorage.setItem("colorLineInnactive", "black");
        localStorage.setItem("colorLineActive", "#2ec22e");
        localStorage.setItem("colorEnd", "#fe8a71");
        localStorage.setItem("colorSwitchInnactiveBackground", "#fe8a71");
        localStorage.setItem("colorSwitchActiveBackground", "#2ec22e");
    }
    colorLineInnactive = localStorage.getItem("colorLineInnactive");
    colorLineActive = localStorage.getItem("colorLineActive");
    colorEnd = localStorage.getItem("colorEnd");
    colorSwitchInnactiveBackground = localStorage.getItem("colorSwitchInnactiveBackground");
    colorSwitchActiveBackground = localStorage.getItem("colorSwitchActiveBackground");
    document.querySelector("#active_switch_picker").value = colorSwitchActiveBackground;
    document.querySelector("#inactive_switch_picker").value = colorSwitchInnactiveBackground;
    document.querySelector("#active_line_picker").value = colorLineActive;
    document.querySelector("#inactive_line_picker").value = colorLineInnactive;
    document.querySelector("#inactive_end_picker").value = colorEnd;
    insertName(pseudo);
    document.querySelector("#pseudo_show").innerHTML = pseudo;
    document.querySelector("#active_switch_picker").value = colorSwitchActiveBackground;
    document.querySelector("#inactive_switch_setting").value = colorSwitchInnactiveBackground;
}

function backHome() {
    closeGame();
    document.querySelector("#lose_pannel").style.display = "none";
    hideDiv(document.querySelector("#playPage"));
    showDiv(document.querySelector("#mainPage"));
}

var mobile = false;
var switchs = [],
    lineCount = [],
    logiques = [],
    lines = [],
    endLines = [],
    end, switchsInfo = [],
    switchsInfoCopy = [],
    circles = [],
    lineRemove = [];
var width = window.innerWidth - window.innerWidth / 100 * 30;
var height = window.innerHeight / 2.5;
var stage = new Konva.Stage({
    container: 'setting_container',
    width: width,
    height: height,
});
var layer = new Konva.Layer();

function tutoCreateLayer() {
    endHeight = 50;
    isTuto = true;
    width = window.innerWidth - window.innerWidth / 100 * 30;
    height = window.innerHeight / 2.5;
    stage = new Konva.Stage({
        container: 'setting_container',
        width: width,
        height: height,
    });
    stage.add(layer);

    colonneTot = 1;
    numberPerColonne = [1];
    var logiqueCount = 0;

    for (let i = 0; i < colonneTot; i++) {
        liveColonneNumber.push([]);
    }
    insertLogiqueColonne("logique1", "et", 0);
    calculNombreSwitch();
    switchCreator(numberOfSwitch);
    createAllLinkSwitch();

    initAllSwitch();
    createEnd();
    initEnd();
}

width = (window.innerWidth - window.innerWidth / 100 * 10);
height = window.innerHeight / 1.5;

function resetAllTabs(save) {
    if (!save) {
        timeEnd = 5;
    }
    logiques = [];

    clickTot = clickTot + click;
    click = 0;

    numberPerColonne = [];
    liveColonneNumber = [];



    numberOfSwitch = 0;

    colonneTot = 0;
    switchs = [], lineCount = [], lines = [], endLines = [], end, switchsInfo = [], lineRemove = [], switchsInfoCopy = [];
    layer.destroyChildren();
    stage.draw();
}


function initEndGame(mode) {

    calculNombreSwitch();
    switchCreator(numberOfSwitch);

    createAllLinkSwitch();
    initAllSwitch(mode);
    /*logiques.forEach(function (element) {
        checkEntreCroisement(element.name);
    });*/

    createEnd();
    initEnd();

    initTimer(mode);

    checkAllSortieLogique();
}

function resetCache() {
    openModal("reset_cache");
    window.localStorage.clear();

    setTimeout(() => {

        location.reload();
    }, 3000);
}

window.addEventListener('resize', saveStage);

function getSwitchState() {
    var stateSwitch = new Array();
    switchsInfoCopy.forEach(function (element) {
        if (stage.findOne("#" + element.id).fill() == colorSwitchActiveBackground) stateSwitch.push(1);
        else stateSwitch.push(0);
    });
    return stateSwitch;
}

function saveStage() {
    var oldSwitch = getSwitchState();
    resetAllTabs(true);
    if (isTuto) {
        createStage("setting_container");
    } else {
        createStage("play_container");
    }

    stage.add(layer);
    resetAllTabs(true);
    colonneTot = tempCol.length;
    for (let i = 0; i < colonneTot; i++) {
        var pos = 1;
        tempPortes.forEach(function (logique) {
            if (logique.col - 1 === i) {
                createLogique(stage.width() / (colonneTot + 1) * (i + 1), (stage.height() - stage.height() / 100 * 25) / (tempCol[i] + 1) * pos, logique.id, logique.type)
                pos++;
            }
        });
    }
    tempLinks.forEach(function (link) {
        if (link[1] !== "end")
            createLink(findLogique(link[0]), findLogique(link[1]), link[2]);
    });
    //On place les rond si fork :
    //On check la valeur entre 2 colonne :
    var d1 = 0, tot = 0;
    logiques.forEach(function (logique) {
        if (d1 === 0) d1 = logique.x;
        else if (d1 !== logique.x && tot === 0) {
            tot = logique.x - d1;
        }
    });
    tempCircles.forEach(function (cir) {
        logiques.forEach(function (logique) {

            if (logique.name === cir[1]) {
                var circle = new Konva.Circle({
                    id: cir[0],
                    x: logique.x + imageWidth + (tot / 100 * pourcentageBreak),
                    y: logique.y + imageHeight / 2,
                    radius: 10,
                    fill: 'black',
                    stroke: 'black',
                    strokeWidth: 4,
                });
                layer.add(circle);
                stage.draw();
            }
        })
    });
    //On init la fin
    initEndGame(tempMode);

    //On change les lignes de places :

    logiques.forEach(function (logique) {
        if (logique.type !== "inv") {
            var distance1;
            var distance2;
            if (getLogiqueFromLine(logique.id1) === null)
                distance1 = getSwitchFromLine(logique.id1).y;
            else distance1 = getLogiqueFromLine(logique.id1).y;
            if (getLogiqueFromLine(logique.id2) === null)
                distance2 = getSwitchFromLine(logique.id2).y;
            else distance2 = getLogiqueFromLine(logique.id2).y;

            if (distance2 < distance1) {

                if (getLogiqueFromLine(logique.id1) === null && getLogiqueFromLine(logique.id2) === null) {

                    let switch1Id = getSwitchFromLine(logique.id1).id;
                    let switch2Id = getSwitchFromLine(logique.id2).id;
                    removeLine(logique.id1);
                    removeLine(logique.id2);
                    logique.id1 = null;
                    logique.id2 = null;
                    createLink(stage.findOne("#" + switch2Id), logique);
                    createLink(stage.findOne("#" + switch1Id), logique);

                } else if (getLogiqueFromLine(logique.id1) !== null && getLogiqueFromLine(logique.id2) !== null) {
                    let log1 = getLogiqueFromLine(logique.id1).name;
                    let log2 = getLogiqueFromLine(logique.id2).name;
                    removeLine(logique.id1);
                    removeLine(logique.id2);
                    logique.id1 = null;
                    logique.id2 = null;
                    createLink(findLogique(log1), logique, "id2");
                    createLink(findLogique(log2), logique, "id1");
                }
            }
        }
    });
}

function showInfo() {
    if (document.querySelector("#info_check_input").checked) allowedInfoPorte = false;
    else allowedInfoPorte = true;

}