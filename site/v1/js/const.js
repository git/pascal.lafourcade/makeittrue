//Portes Logiques
var imageHeight = 75 / 2;
var imageWidth = 50;
var imageRotation = 0;

//couleur
const blackColor = "#444442";
const whiteColor = "#D7CCC8";
//Image
var pathImg = "img/txt_en/";
const imageLogiqueEt = "and.png";
const imageLogiqueOu = "or.png";
const imageLogiqueEtNon = "nand.png";
const imageLogiqueInv = "not.png";
const imageLogiqueNonOu = "nor.png";

//line
var colorLineInnactive = "black";
var colorLineActive = "#2ec22e"; //FFF033
const lineStroke = 4;
const pourcentageBreak = 15;
const tension = 0;

//objectif
var colorEnd = "#fe8a71";
var endHeight = 200;

//switch
const SwitchHeight = 25;
const SwitchWidth = 50;
const colorSwitchBorder = "black";
var colorSwitchInnactiveBackground = "#fe8a71";
var colorSwitchActiveBackground = "#2ec22e";

//timer
var timeEnd = 10;
var timeTot = 0;
var time;

//switch
var numberOfSwitch = 0;

//grid
var colonneTot = 4;
var numberPerColonne = [];
var liveColonneNumber = [];

var niveauActuel = 1;

var isTuto = true;
var isTutoriel = false;

//window

var windowWidth = 0;
var windowHeight = 0;
var modeG = null;
var mobile = false;

var allowedInfoPorte = true;

var double = false;

var click = 0, perfectClick = 0, clickTot = 0;

var successPanel = true;

var arrayPorte = [];
var pseudo = null;
const NOMBRETEST = 500;
var isFinish = false;
var tempCol = [], tempMode = null, tempLinks = [], tempPortes = [], changePorte = [], tempCircles = [];