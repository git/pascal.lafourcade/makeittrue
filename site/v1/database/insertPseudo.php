
<?php

require('db.php');

$pseudo = $_REQUEST["pseudo"];

$req = $db->prepare("SELECT * FROM pseudos WHERE pseudo=:pseudo");
$req->bindValue(':pseudo', $pseudo, SQLITE3_TEXT);
$results = $req->execute();

if ($results->fetchArray()[0] == null) {
    $req = $db->prepare("INSERT INTO pseudos (pseudo) VALUES (:pseudo) ");
    $req->bindValue(':pseudo', $pseudo, SQLITE3_TEXT);
    $req->execute();
}
