
<?php

require('db.php');

$newPseudo = $_REQUEST["newPseudo"];
$oldPseudo = $_REQUEST["oldPseudo"];

$req = $db->prepare("UPDATE scores SET pseudo=:newPseudo WHERE pseudo=:oldPseudo");
$req->bindValue(':newPseudo', $newPseudo, SQLITE3_TEXT);
$req->bindValue(':oldPseudo', $oldPseudo, SQLITE3_TEXT);
$req->execute();

$req = $db->prepare("UPDATE pseudos SET pseudo=:newPseudo WHERE pseudo=:oldPseudo");
$req->bindValue(':newPseudo', $newPseudo, SQLITE3_TEXT);
$req->bindValue(':oldPseudo', $oldPseudo, SQLITE3_TEXT);
$req->execute();
