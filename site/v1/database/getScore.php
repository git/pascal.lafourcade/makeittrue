<?php
require('db.php');

$mode = $_REQUEST["mode"];
$req = $db->prepare('SELECT pseudo,score FROM scores WHERE mode = :mode ORDER BY score DESC ');
$req->bindValue(':mode', $mode, SQLITE3_TEXT);

$results = $req->execute();
$i = 1;
while ($row = $results->fetchArray()) {
    echo $i . " : " . $row['pseudo'] . " | " . $row['score'] . "<br>";
    $i = $i + 1;
}
