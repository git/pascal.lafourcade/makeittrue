
<?php

require('db.php');

$pseudo = $_REQUEST["pseudo"];
$mode = $_REQUEST["mode"];
$temps = $_REQUEST["temps"];
$clicks = $_REQUEST["clicks"];
$score = $_REQUEST["score"];

//On va chercher l'ancien score
$req = $db->prepare('SELECT score FROM scores WHERE pseudo = :pseudo AND mode = :mode');
$req->bindValue(':pseudo', $pseudo, SQLITE3_TEXT);
$req->bindValue(':mode', $mode, SQLITE3_TEXT);
$results = $req->execute();

if ($results->fetchArray()[0] == null) {
    $req = $db->prepare('INSERT INTO scores(pseudo,mode,temps,clicks,score) VALUES(:pseudo,:mode,:temps,:clicks,:score)');
    $req->bindValue(':pseudo', $pseudo, SQLITE3_TEXT);
    $req->bindValue(':mode', $mode, SQLITE3_TEXT);
    $req->bindValue(':temps', $temps, SQLITE3_FLOAT);
    $req->bindValue(':clicks', $clicks, SQLITE3_INTEGER);
    $req->bindValue(':score', $score, SQLITE3_FLOAT);
    $req->execute();
} else {
    if (floatval($results->fetchArray()[0]) < floatval($score)) {
        $req = $db->prepare("UPDATE scores SET score=:score, clicks=:clicks,temps=:temps WHERE pseudo=:pseudo AND mode=:mode");
        $req->bindValue(':pseudo', $pseudo, SQLITE3_TEXT);
        $req->bindValue(':mode', $mode, SQLITE3_TEXT);
        $req->bindValue(':temps', $temps, SQLITE3_FLOAT);
        $req->bindValue(':clicks', $clicks, SQLITE3_INTEGER);
        $req->bindValue(':score', $score, SQLITE3_FLOAT);
        $req->execute();
    }
}




?>
