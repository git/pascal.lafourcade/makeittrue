<?php

require('db.php');

$pseudo = $_REQUEST["pseudo"];

//On check si le pseudo existe déjà
$req = $db->prepare('SELECT pseudo FROM pseudos WHERE pseudo = :pseudo');
$req->bindValue(':pseudo', $pseudo, SQLITE3_TEXT);
$results = $req->execute();

if ($results->fetchArray()[0] == null) {
    echo "0";
} else {
    echo "1";
}
