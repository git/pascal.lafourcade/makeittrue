<?php
	class FrontControleur
	{

		function __construct()
		{
			global $rep,$vues; 
			session_start();
			$dVueErreur = array();
			try
			{
				$action = $_REQUEST['action'];
				
				$acteur = $this->isGoodAction($action);

				if(empty($acteur))
				{
					$dVueErreur[] = 'Action invalide';
					require('pageErreur');
				}
				elseif($acteur == 'FrontControleur')
				{
					switch ($action) {
						case 'Play' :
							$this->Play($dVueErreur);
							break;
					}
				}
				else
				{
					$controleur = new $acteur();
				}
			}
			catch(Exception $ex)
			{
				switch($action)
				{
					case 'Play' :
						require ($rep.$vues['register']);
						break;
				}
			}
		}

		function isGoodAction($action)
		{
			$listeActions=array(
					'Player' => array('Play', NULL),
				);

			if(in_array($action, $listeActions['Player']))
			{
				return 'PlayerControleur';
			}
			else
				return;
		}

		

		/*function Login(array &$dVueErreur)
		{
			global $rep,$vues;
			$email = htmlspecialchars($_POST['email']);
			$password = htmlspecialchars($_POST['password']);
			Validation::isEmailException($email,$dVueErreur);
			Validation::isPasswordMatchException($password, $dVueErreur);
			$model = new UserManager();
			$retour = $model->connect($email, $password, $dVueErreur);
			$controleur = new VisiteurControleur();
			$controleur->GoToListe($dVueErreur);

		}


		function LogOut(array &$dVueErreur)
		{
			global $rep,$vues;
			$model = new UserManager();
			$model->disconnect();
			$controleur = new VisiteurControleur();
			$controleur->GoToListe($dVueErreur);
		}*/
	}
?>