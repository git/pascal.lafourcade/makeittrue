<?php

class PlayerControleur{

	function __construct() {
		global $rep,$vues, $previousPage;
		$dVueEreur = array ();
		$action=$_REQUEST['action'];
		try{
			switch($action) {
				case "Play" :
					$this->Play($dVueEreur);
					break;
			}

		} catch (PDOException $e)
		{

			$dVueErreur[] =	"Erreur inattendue!!! ";
			require ($rep.$vues['liste']);

		}
		catch (Exception $e2)
		{
			switch ($action) {
				case 'CreerToDoListU':
					$controleur= new VisiteurControleur();
					$controleur->GoToListe($dVueEreur);
					break;

				case 'PasserToDoListPublique' : 
					$controleur= new VisiteurControleur();
					$controleur->GoToListe($dVueEreur);
					break;

				case 'PasserToDoListPrive' : 
					$controleur= new VisiteurControleur();
					$controleur->GoToListe($dVueEreur);
					break;

				default:
					$controleur= new VisiteurControleur();
					$controleur->GoToListe($dVueEreur);
					break;
			}
		}

	}

	function Play(array &$dVueErreur)
		{
			global $rep,$vues;
			$email = htmlspecialchars($_POST['email']);
			$password1 = htmlspecialchars($_POST['password1']);
			$password2 = htmlspecialchars($_POST['password2']);
			$pseudo = htmlspecialchars($_POST['pseudo']);
			Validation::isVideException($pseudo,$dVueErreur);
			Validation::isEmailException($email,$dVueErreur);
			Validation::isPasswordValidException($password1, $password2,$dVueErreur);
			$model = new UserManager();
			$model->createUser($email, $password1, $pseudo, $dVueErreur);
			$controleur = new VisiteurControleur();
			$controleur->GoToLogin();
		}
}