function initAllSwitch() {
  switchs.forEach(function (element) {
    let switche = stage.findOne("#" + element);
    switche.on('click', function () {
      
      let colorrect = switche.fill() == colorSwitchInnactiveBackground ? colorSwitchActiveBackground : colorSwitchInnactiveBackground;
      switche.fill(colorrect);
      changeLineColor(switche.id3);
      checkAllSortieLogique();
      layer.draw();
      checkEnd(); 
    });
    switche.on('mouseover', function () {
      document.body.style.cursor = "pointer";
    });
    switche.on('mouseout', function () {
      document.body.style.cursor = "default";
    });

  });
   
}
function initLayer() {
  
}

function compareLogiqueForEnd(a,b){
  if(a.y > b.y)
    return -1;
  if(a.y < b.y)
    return 1;
  return 0;
}

function initEnd() {
  var listeLogique = [];
  let countLogique = 0;
  logiques.forEach(function (element) {
    if(element.id3 == null){
      countLogique++;
      listeLogique.push(element);
    }
  });
  listeLogique.sort(compareLogiqueForEnd);
  end.position = countLogique;
  end.let = countLogique;
  listeLogique.forEach(function (element) {
    if(element.id3 == null){
      createLink(element,end);
    }
  });
}