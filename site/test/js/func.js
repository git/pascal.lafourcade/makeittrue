function whatIsElement(element) {
  if (findLogique(element.name) == null || findLogique(element.name) == undefined) {
    return "switch";
  } else {
    return "logique";
  }
}

function setLine(logiqueElement, lineId, lineName) {
  logiques.forEach(function (element, index) {
    if (element.name === logiqueElement.name) {
      Object.keys(element).map(function (objectKey, index) {
        if (objectKey == lineId) {
          element[objectKey] = lineName;
        }
      });
    }
  });
}

function changeLineColor(idLine) {
  let line = stage.findOne("#" + idLine);
  var colorline = line.stroke() == colorLineActive ? colorLineInnactive : colorLineActive;
  line.stroke(colorline);
}
function changeLineColorBlack(idLine) {
  
  let line = stage.findOne("#" + idLine);
  line.stroke(colorLineInnactive);
}
function changeLineColorYellow(idLine) {
  let line = stage.findOne("#" + idLine);
  line.stroke(colorLineActive);
}

function checkAllSortieLogique() {
  logiques.forEach(function (element) {
    if (element.id3 != null)
      checkSortieLogique(element.name);
  });
}

function checkSortieLogique(logiqueId) {
  let logique = findLogique(logiqueId);
  logiques.forEach(function (element, index) {
    if (element.name === logique.name) {
      let line1State, line2State;
      let id1Color = stage.findOne("#" + element.id1).stroke();
      if (element.type !== "inv") {
        let id2Color = stage.findOne("#" + element.id2).stroke();
        if (id2Color == colorLineInnactive) {
          line2State = false;
        } else {
          line2State = true;
        }
      }

      if (id1Color == colorLineInnactive) {
        line1State = false;
      } else {
        line1State = true;
      }

      switch (logique.type) {
        case "et":
          if (line1State == true && line2State == true) {
            changeLineColorYellow(element.id3);
          }
          else {
            changeLineColorBlack(element.id3);
          }
          break;
        case "inv":
          if (line1State == false) {
            changeLineColorYellow(element.id3);
          }
          else {
            changeLineColorBlack(element.id3);
          }
          break;
        case "ou":
          if (line1State == true || line2State == true) {
            changeLineColorYellow(element.id3);
          }
          else {
            changeLineColorBlack(element.id3);
          }
          break;
          case "nonou":
            if (line1State == true || line2State == true) {
              changeLineColorBlack(element.id3);
            }
            else {
              changeLineColorYellow(element.id3);
            }
            break;
        case "etnon":
          if (line1State == false && line2State == false) {
            changeLineColorYellow(element.id3);
          } else if (line1State == false && line2State == true) {
            changeLineColorYellow(element.id3);
          } else if (line1State == true && line2State == false) {
            changeLineColorYellow(element.id3);
          } else {
            changeLineColorBlack(element.id3);
          }
          break;
      }
    }
  });
}

function getRandomArbitrary(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}

function isElementExisting(elementId) {
  if (stage.findOne("#" + elementId) != null) {
    return true;
  }
  return false;
}

function checkLineSwitch(switchId) {
  if (stage.findOne("#" + switchId) != null) {
    return true;
  }
  return false;
}

function checkEnd() {
  let ter = true;
  endLines.forEach(function (element) {
    if (stage.findOne("#line" + element).stroke() == "black") {
      ter = false;

    }
  });
  if (ter) {
    setTimeout(() => { localStorage.setItem("niveau", parseInt(niveauActuel)+1); document.location.reload(true); }, 100);
  }
}


function calculNombreSwitch(){
  var entrelibre = 0;
  logiques.forEach(function(element){
    
    if(element.id1 == null){
      entrelibre++;
    }
    if(element.id2 == null && element.type != "inv"){
      entrelibre++;
    }
  });
  numberOfSwitch = entrelibre;
}



function resize(){
  console.log(window.innerWidth);
  stage.scale({ x: 1, y: 1 });
  stage.draw();
  console.log(stage);
}


