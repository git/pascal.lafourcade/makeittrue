function initTimer(){
    var TimerElem = document.getElementById("progressBar");
    TimerElem.style.width = "0%";
    let seconds = timeEnd;
    let minutes = 0;
    while(seconds - 60 > 0){
      minutes++;
      seconds = seconds - 60;
    }
    document.getElementById("timerend").innerHTML = " / " + minutes +" m "+ seconds + " s";
}

function move() {
    var lapsPerSecond = 100/timeEnd;
    var TimerElem = document.getElementById("progressBar");
    TimerElem.style.width = (parseInt(TimerElem.style.width) + lapsPerSecond)+"%";
}



var timer = new easytimer.Timer();
    timer.start();
    timer.addEventListener('secondsUpdated', function (e) {
      if(timer.getTimeValues().seconds <= timeEnd){
        document.getElementById("timer").innerHTML = timer.getTimeValues().minutes.toString() +" m " + timer.getTimeValues().seconds.toString() + " s";
        move();
      }else{
        endTime();
        timer.stop();
      }
    });
    function endTime(){
        var modal = document.getElementById("myModal");
        modal.style.display = "block";
    }