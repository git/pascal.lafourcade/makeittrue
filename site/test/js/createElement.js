function createEnd() {

  //find X and Y

  var X = stage.width() - stage.width() /100 * 5;
  var Y = stage.height() / 3;

  var imageObj2 = new Image();
  imageObj2.onload = function () {
    var end = new Konva.Image({
      x: X,
      y: Y,
      image: imageObj2,
      width: 50,
      height: 50,
      id: "end",
    });
    layer.add(end);
    layer.batchDraw();
  };
  end = {
    name: "end",
    x: X,
    y: Y,
    type: "end",
    position: 1,
    let:1,
  }
  imageObj2.src = pathImg + imageEnd;
}

function createSwitch(id, x, y) {
  var switche = {
    id : id,
    x : x,
    y : y,
  };
  switchsInfo.push(switche);
  switchsInfoCopy.push(switche);
  switchs.push(id);
  var num = lineCount.length;
  lineCount.push(1);
  var rect1 = new Konva.Rect({
    x: x,
    y: y,
    width: SwitchWidth,
    height: SwitchHeight,
    stroke: colorSwitchBorder,
    fill: colorSwitchInnactiveBackground,
    strokeWidth: 4,
    id: id,
    id3: null,
  });
  layer.add(rect1);
}

function createLine(points, id) {
  let startX = points[0], startY = points[1], endX = points[2], endY = points[3];
  let distance20 = (endX - startX)/100*pourcentageBreak;
  let newPoints = [startX, startY,startX + distance20, startY,endX - distance20,endY,endX,endY];
  let line = new Konva.Line({
    points: newPoints,
    fill : 'blue',
    stroke: colorLineInnactive,
    strokeWidth: lineStroke,
    id: id,
  });
  layer.add(line);
}


function createLink(entre, sortie) {
  //entre est une porte logique
  lineId = lineCount.length;
  lineCount.push(1);
  switch (whatIsElement(entre)) {
    case "switch":
      switch (whatIsElement(sortie)) {
        case "logique":
          switch (giveLineId(sortie.name,entre)) {
            
            case "id1":
              let switchMidX = entre.getX() + entre.getWidth();
              let switchMidY = entre.getY() + entre.getHeight() / 2;

              let logiqueMidX = sortie.x;
              if(sortie.type === "inv"){
                var logiqueMidY = sortie.y + (imageHeight / 2);
              }else{
                var logiqueMidY = sortie.y + (imageHeight / 3);
              }
              createLine([switchMidX, switchMidY, logiqueMidX, logiqueMidY], "line" + lineId);
              setLine(sortie, "id1", "line" + lineId);
              entre.id3 = "line" + lineId;
              break;
            case "id2":
              createLine([entre.getX() + entre.getWidth(), entre.getY() + entre.getHeight() / 2, sortie.x, sortie.y + (imageHeight / 3) * 2], "line" + lineId);
              setLine(sortie, "id2", "line" + lineId);
              entre.id3 = "line" + lineId;
              break;
          }
      }
      break;
    case "logique":
      switch (whatIsElement(sortie)) {
        case "logique":
          switch (giveLineId(sortie.name,entre)) {
            case "id1":
              let switchMidX = entre.x + imageWidth;
              let switchMidY = entre.y + imageHeight / 2;

              let logiqueMidX = sortie.x;
              if(sortie.type === "inv"){
                var logiqueMidY = sortie.y + (imageHeight / 2);
              }else{
                var logiqueMidY = sortie.y + (imageHeight / 3);
              }
              createLine([switchMidX, switchMidY, logiqueMidX, logiqueMidY], "line" + lineId);
              setLine(sortie, "id1", "line" + lineId);
              setLine(entre, "id3", "line" + lineId);
              break;
            case "id2":
              createLine([entre.x + imageWidth, entre.y + imageHeight / 2, sortie.x, sortie.y + (imageHeight / 3) * 2], "line" + lineId);
              setLine(sortie, "id2", "line" + lineId);
              setLine(entre, "id3", "line" + lineId);
              break;
          }
          break;
        default:
          console.log("id2222222222222222222");
          endLines.push(lineId);
          createLine([entre.x + imageWidth, entre.y + imageHeight / 2, sortie.x, sortie.y + (imageHeight / end.position)*end.let], "line" + lineId);
          setLine(entre, "id3", "line" + lineId);
          end.let--;
          break;
      }
      break;
      
  }
}
function createLogique(x, y, id, type) {
  var imageObj = new Image();
  imageObj.onload = function () {
    var et = new Konva.Image({
      x: x,
      y: y,
      image: imageObj,
      width: imageWidth,
      height: imageHeight,
      id: id,
    });
    if (imageRotation != 0) {
      et.rotation(imageRotation);
      et.x(et.getX() + imageHeight);
    }
    layer.add(et);
    layer.batchDraw();
  };
  switch(type){
    case "et" :
      imageObj.src = pathImg + imageLogiqueEt;
      break;
    case "ou":
      imageObj.src = pathImg + imageLogiqueOu;
      break;
    case "etnon":
      imageObj.src = pathImg + imageLogiqueEtNon;
      break;
    case "inv":
      imageObj.src = pathImg + imageLogiqueInv;
      break;
    case "nonou":
      imageObj.src = pathImg + imageLogiqueNonOu;
      break;
  }
  
  imageObj.id = id;



  var logique = {
    name: id,
    x: x,
    y: y,
    type: type,
    id1: null,
    id2: null,
    id3: null,
  }
  logiques.push(logique);
}

function switchCreator(number){
  numberOfSwitch = number;
  let split = number + 1;
  let space = (stage.height() - stage.height()/100*25) / split;
  for(let i = 1; i < number+1 ; i++){
    createSwitch("s"+i, 20, (i) * space);
  }
}

function insertLogiqueColonne(nomLogique, TypeLogique, colonne){
  console.log(TypeLogique);
  var X = stage.width() / (colonneTot+1) * (colonne+1);
  //calcul position Y
  if(numberPerColonne[colonne] == 1){
    var Y = stage.height() / 3;
  }else{
    let numberAlreadyAdded = liveColonneNumber[colonne].length+1;
    let split = (stage.height() - stage.height()/100*25) / (numberPerColonne[colonne]+1);
    var Y = split * numberAlreadyAdded;
  }
  createLogique(X, Y, nomLogique,TypeLogique);
  liveColonneNumber[colonne].push(1);
}

function createLinkSwitch(){
  var logique = null;
  var distance = stage.width();
  //Trouver la porte logique la plus proche des switchs
  logiques.forEach(function(element){
    if(element.id1 == null || element.id2 == null){
      if(element.x < distance){
        logique = element;
        distance = element.x;
      }
    }
  });
  var distanceSwitch = stage.height();
  var switchs = null;
  if(switchsInfo.length > 0){
    switchsInfo.forEach(function(element){
      if(Math.abs(logique.y - element.y) < distanceSwitch){
        distanceSwitch = Math.abs(logique.y - element.y);
        switchs = element;
      }
    });
  }
  else {
    switchsInfoCopy.forEach(function(element){
      if(stage.findOne("#"+element.id).id3 == null){
        if(Math.abs(logique.y - element.y) < distanceSwitch){
          distanceSwitch = Math.abs(logique.y - element.y);
          switchs = element;
        }
      }
    });
  } 
  
  switchsInfo.splice( switchsInfo.indexOf(switchs), 1 );
  createLink(stage.findOne("#"+switchs.id), findLogique(logique.name));
}

function getNumberOfSwitchEmpty(){
  let number = 0;
  switchsInfoCopy.forEach(function(element){
    if(stage.findOne("#"+element.id).id3 == null)
      number++;
  });
  return number;
}

function createAllLinkSwitch(){
  while(getNumberOfSwitchEmpty() > 0){
    createLinkSwitch();
  }
}

function createLinkAuto(logiqueId){
  var logique = findLogique(logiqueId);
  var ligneSupCoor = null;
  var stop = false;
  //on cherche la ligne supérieur
  logiques.forEach(function(element){
    if(!stop){
      if(logique.x < element.x){
        stop = true;
        ligneSupCoor = element.x;
      }
    }
  });
  if(ligneSupCoor != null){
    //on stock les portes logiques de la lignes supérieur (si il reste de la place dans leur entrees)
    var ligneSup = [];
    logiques.forEach(function(element){
      
      if(element.x == ligneSupCoor){
        if(checkLogiqueLines(element.name,"id1") == false || checkLogiqueLines(element.name,"id2") == false){
          ligneSup.push(element);
        }
      }
    });
    //on tire au sort
    let sortie = getRandomArbitrary(0,ligneSup.length);
    sortie = ligneSup[sortie];
    createLink(findLogique(logiqueId),findLogique(sortie.name));
  }
}