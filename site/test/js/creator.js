function createGame1() {

    timeEnd = 20;

    initTimer();

    createSwitch("s1", 20, 20);
    createSwitch("s2", 20, 90);
    createSwitch("s3", 20, 300);
    createSwitch("s4", 20, 500);
    createSwitch("s5", 20, 700);
    createSwitch("s6", 20, 600);
    createSwitch("s7", 350, 600);

    createLogique(200, 100, "logique1", "et");
    createLogique(300, 200, "logique2", "nonou");
    createLogique(500, 300, "logique3", "et");
    createLogique(500, 400, "logique4", "etnon");
    createLogique(500, 500, "logique5", "inv");

    createLink(stage.findOne("#s7"), findLogique("logique5"));
    createLink(stage.findOne("#s5"), findLogique("logique4"));
    createLink(stage.findOne("#s6"), findLogique("logique4"));
    createLink(stage.findOne("#s3"), findLogique("logique2"));
    createLink(stage.findOne("#s2"), findLogique("logique1"));
    createLink(stage.findOne("#s1"), findLogique("logique1"));
    createLink(findLogique("logique1"), findLogique("logique2"));
    createLink(stage.findOne("#s4"), findLogique("logique3"));
    createLink(findLogique("logique2"), findLogique("logique3"));
    initAllSwitch();
    createEnd(800, 320);
    initEnd();
}
function createGame2() {

    timeEnd = 40;

    colonneTot = 3;
    numberPerColonne = [1,1,3];

    initTimer();

    switchCreator(7);

    insertLogiqueColonne("logique1", "et", 0);
    insertLogiqueColonne("logique2", "nonou", 1);
    insertLogiqueColonne("logique3", "et", 2);
    insertLogiqueColonne("logique4", "etnon", 2);
    insertLogiqueColonne("logique5", "inv", 2);

    createLink(findLogique("logique1"), findLogique("logique2"));
    createLink(findLogique("logique2"), findLogique("logique3"));

    createAllLinkSwitch();

    initAllSwitch();
    createEnd();
    initEnd();
}

function tuto(){
    timeEnd = 1000;

    initTimer();

    var logiqueCount = 0;

    colonneTot = 1;

    for (let i = 0; i < colonneTot; i++) {
        liveColonneNumber.push([]);
    }

    numberPerColonne[0] = 1;

    insertLogiqueColonne("logique" + logiqueCount, "et", 0);
    

    logiques.forEach(function (element) {
        createLinkAuto(element.name);
    });

    calculNombreSwitch();
    switchCreator(numberOfSwitch);

    createAllLinkSwitch();

    initAllSwitch();
    createEnd();
    initEnd();
}

function creatorRandomPyramid(){
    timeEnd = getRandomArbitrary(15, 50);

    initTimer();

    var premiereColonne = 2;

    let still = premiereColonne;
    let nmbColonne = 1;

    while(still != 1){
        still = still / 2;
        nmbColonne++;
    }

    var logiqueCount = 0;

    colonneTot = nmbColonne;

    for (let i = 0; i < colonneTot; i++) {
        liveColonneNumber.push([]);
    }
    premiereColonne = premiereColonne * 2;
    for (let i = 0; i < colonneTot; i++) {
        numberPerColonne[i] = premiereColonne - premiereColonne / 2;
        premiereColonne = premiereColonne / 2;
        for (let j = 0; j < numberPerColonne[i]; j++) {
            logiqueCount++;
            let type = null;
            switch (getRandomArbitrary(0, 4)) {
                case 0:
                    type = "et";
                    break;
                case 1:
                    type = "ou";
                    break;
                case 2:
                    type = "etnon";
                    break;
                case 3:
                    type = "nonou";
                    break;
            }
            insertLogiqueColonne("logique" + logiqueCount, type, i);
        }
    }
    
    logiques.forEach(function (element) {
        createLinkAuto(element.name);
    });

    calculNombreSwitch();
    switchCreator(numberOfSwitch);

    createAllLinkSwitch();

    initAllSwitch();
    createEnd();
    initEnd();
}

function generatorGame() {

    timeEnd = getRandomArbitrary(15, 50);

    initTimer();

    var logiqueCount = 0;

    colonneTot = getRandomArbitrary(2, 3);

    for (let i = 0; i < colonneTot; i++) {
        liveColonneNumber.push([]);
    }

    for (let i = 0; i < colonneTot; i++) {
        numberPerColonne[i] = getRandomArbitrary(2, 4);
        for (let j = 0; j < numberPerColonne[i]; j++) {
            logiqueCount++;
            let type = null;
            switch (getRandomArbitrary(0, 4)) {
                case 0:
                    type = "et";
                    break;
                case 1:
                    type = "ou";
                    break;
                case 2:
                    type = "etnon";
                    break;
                case 3:
                    type = "nonou";
                    break;
            }
            insertLogiqueColonne("logique" + logiqueCount, type, i);
        }
    }
    
    logiques.forEach(function (element) {
        createLinkAuto(element.name);
    });

    calculNombreSwitch();
    switchCreator(numberOfSwitch);

    createAllLinkSwitch();

    initAllSwitch();
    createEnd();
    initEnd();

}