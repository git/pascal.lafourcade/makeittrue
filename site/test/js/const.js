//Portes Logiques
var imageHeight = 70;
var imageWidth = 75;
var imageRotation = 0;

//couleur
const blackColor = "#444442";

//Image
const pathImg = "../img/";
const imageLogiqueEt = "eee.png";
const imageLogiqueOu = "zzz.png";
const imageLogiqueEtNon = "nand.png";
const imageLogiqueInv = "not_test.png";
const imageLogiqueNonOu = "aaa.png";
const imageEnd = "idea_white.png";

//line
const colorLineInnactive = "black";
const colorLineActive = "#4CFEFE";//FFF033
const lineStroke = 4;
const pourcentageBreak = 15;

//switch
const SwitchHeight = 25;
const SwitchWidth = 50;
const colorSwitchBorder = "black";
const colorSwitchInnactiveBackground = "red";
const colorSwitchActiveBackground = "green";

//timer
var timeEnd = 70;

//switch
var numberOfSwitch = 0;

//grid
var colonneTot = 4;
var numberPerColonne = [];
var liveColonneNumber = [];